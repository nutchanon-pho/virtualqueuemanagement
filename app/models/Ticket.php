<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Ticket extends Eloquent {


	public $fillable = ['user_id', 'position', 'date', 'active'];
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'queues';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password');
	public static function getLastPosition()
	{
		$queue = Ticket::all();
		if($queue->isEmpty()) return 0;
		$lastTicket = Ticket::all()->last();
		return $lastTicket->position;
	}

	public static function getHeadOfQueue()
	{
		return Ticket::all()->first();
	}
}
