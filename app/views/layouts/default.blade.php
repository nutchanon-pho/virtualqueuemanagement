<!DOCTYPE html>
<html>
	<head>
		<title>Index</title>
		@yield('header')
	</head>
	<body>
		@yield('content')
	</body>
</html>
