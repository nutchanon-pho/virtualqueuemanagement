@extends('layouts.default')

@section('content')
<a href='create'>Add Ticket to Queue</a>
<h1>Queue</h1>
@if($queue->count() == 0)
<p>The queue is empty</p>
@else
<p>Current length of the queue: {{ $queue->count() }}</p>
@foreach ($queue as $ticket)
<p>{{ $ticket }}</p>
@endforeach

{{ Form::open(array('method' 
=> 'DELETE', 'route' => array('queue.destroy', $queue[0]->id))) }}
<div>{{ Form::submit('Dequeue') }}</div>
{{ Form::close() }}

@endif
@stop