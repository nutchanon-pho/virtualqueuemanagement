@extends('layouts.default')

@section('content')
{{ Form::open(['route'=>'queue.store']) }}
<div>
	{{ Form::label('userid', 'userid: ') }}
	{{ Form::input('text', 'userid') }}
	{{ $errors->first('username') }}
</div>
<div>{{ Form::submit('Add Ticket') }}</div>
{{ Form::close() }}
@stop